drop table if exists trip;
drop table if exists automobile;
drop table if exists driver;

create table driver
(
    id             bigserial primary key,
    first_name     char(20) DEFAULT 'DEFAULT_FIRST_NAME',
    last_name      char(20) DEFAULT 'DEFAULT_LAST_NAME',
    phone_number   bigint unique,
    experience     integer check ( experience >= 0)              not null,
    age            integer check ( 0 < age and age < 120)        not null,
    driver_license bool,
    category       char(10) DEFAULT '',
    rating         integer check ( 0 <= rating and rating <= 5 ) not null
);

create table automobile
(
    id         serial primary key,
    model      char(20),
    color      char(20),
    number_car char(10) unique,
    driver_id  integer not null,
    foreign key (driver_id) references driver (id)
);

create table trip
(
    id            serial primary key,
    driver_id     bigint,
    automobile_id bigint,
    foreign key (driver_id) references driver (id),
    foreign key (automobile_id) references automobile (id),
    date_trip     timestamp,
    time_trip     time not null
);