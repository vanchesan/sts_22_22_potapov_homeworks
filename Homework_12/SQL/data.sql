insert into driver (first_name, last_name, phone_number, experience, age, driver_license, category, rating)
values ('����', '�������', 89370000000, 3, 31, true, 'B', 4),
       ('�����', '�������', 89370001111, 15, 48, true, 'B, C, D', 5),
       ('�������', '��������', 89370002222, 0, 18, false, DEFAULT, 0),
       ('�����', '���������', 89370007777, 1, 24, true, 'A, B', 1),
       ('�����', '��������', 89370008888, 20, 55, true, 'B, C, D, E', 5);

insert into automobile (model, color, number_car, driver_id)
values ('Focus', '������', 'A121AA001', 1),
       ('Camry', '�����', 'A001AA001', 4),
       ('Rapid', '�������', '�121��52', 3),
       ('Largus', '������', '�452��52', 2),
       ('Vesta', '����������', '�456A�81', 5);

insert into trip (driver_id, automobile_id, date_trip, time_trip)
values (1, 1, '2023-01-07', '3:00'),
       (2, 4, '2022-12-22', '24:00'),
       (4, 2, '2022-12-23', '7:00'),
       (5, 1, '2023-03-08', '5:00'),
       (3, 3, '2023-02-23', '7:49');
