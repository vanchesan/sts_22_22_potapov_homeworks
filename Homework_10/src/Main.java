import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        int i = 0;
        String str = "Hello Hello bye Hello bye Inno";
        HashMap<String, Integer> hashMap = new HashMap<>();
        String[] words = str.split(" ");

        for (String word : words) {
            Integer frequency = hashMap.get(words[i]);
            hashMap.put(words[i], frequency == null ? 1 : frequency + 1);
            System.out.print(word + " ");
            i++;
        }
        System.out.println();
        Map.Entry<String, Integer> maxEntry = hashMap.entrySet().stream()
                .max(Comparator.comparing(Map.Entry::getValue))
                .orElse(null);
        System.out.print(maxEntry);
    }
}

