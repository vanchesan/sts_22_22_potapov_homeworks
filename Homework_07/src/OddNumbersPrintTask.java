public class OddNumbersPrintTask extends AbstractNumbersPrintTask {
    public OddNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public void complete() {
        if (from <= to) {
            for (int i = from; i <= to; i++) {
                if (Math.abs(i % 2) == 1)
                    System.out.print(i + " ");

            }
        } else {
            for (int i = to; i <= from; i++) {
                if (Math.abs(i % 2) == 1)
                    System.out.print(i + " ");
            }
        }
    }
}
