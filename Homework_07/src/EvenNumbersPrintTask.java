public class EvenNumbersPrintTask extends AbstractNumbersPrintTask {

    public EvenNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public void complete() {
        if (from <= to) {
            for (int i = from; i <= to; i++) {
                if (Math.abs(i % 2) == 0)
                    System.out.print(i + " ");

            }
        } else {
            for (int i = to; i <= from; i++) {
                if (i % 2 == 0)
                    System.out.print(i + " ");
            }
        }
    }
}
