public class Main {
    public static void completeAllTask(Task[] task) {
        for (int i = 0; i < task.length; i++) {
            task[i].complete();
            System.out.println();
        }
    }

    public static void main(String[] args) {
        final int N = 12;
        Task[] task = new Task[N];
        task[0] = new EvenNumbersPrintTask(10, 0);
        task[1] = new OddNumbersPrintTask(10, 0);
        task[2] = new EvenNumbersPrintTask(-10, 0);
        task[3] = new OddNumbersPrintTask(-10, 0);
        task[4] = new EvenNumbersPrintTask(-4, 4);
        task[5] = new OddNumbersPrintTask(-4, 4);
        task[6] = new EvenNumbersPrintTask(1, 1);
        task[7] = new OddNumbersPrintTask(2, 2);
        task[8] = new EvenNumbersPrintTask(2, 2);
        task[9] = new OddNumbersPrintTask(3, 3);
        task[10] = new EvenNumbersPrintTask(0, 10);
        task[11] = new OddNumbersPrintTask(0, 10);

        completeAllTask(task);
    }
}
