import java.util.List;

public interface CarsRepository {
    List<Car> findAll();

    double findAveragePriceCamry();

    List<String> getNumberCarWithPriceAndColorOption();

    String getColorMinPriceCar();

    List<String> uniqueCarFromRangePrice();
}
