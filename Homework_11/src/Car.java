public class Car {
    private String numberCar;

    private String nameCar;
    private String color;
    private Integer mileage;

    private Integer price;

    public Car(String numberCar, String nameCar, String color, Integer mileage, Integer price) {
        this.numberCar = numberCar;
        this.nameCar = nameCar;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getNumberCar() {
        return numberCar;
    }

    public String getNameCar() {
        return nameCar;
    }

    public String getColor() {
        return color;
    }

    public Integer getMileage() {
        return mileage;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "numberCar='" + numberCar + '\'' +
                ", nameCar='" + nameCar + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", prise=" + price +
                '}';
    }
}
