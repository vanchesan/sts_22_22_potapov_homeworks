public class UnsuccesfulWorkWithFileException extends RuntimeException {
    public UnsuccesfulWorkWithFileException(Exception e) {
        super(e);
    }
}
