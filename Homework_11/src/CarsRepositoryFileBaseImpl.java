import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryFileBaseImpl implements CarsRepository {

    private final String fileName;

    public CarsRepositoryFileBaseImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Car> carMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String numberCar = parts[0];
        String nameCar = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer price = Integer.parseInt(parts[4]);
        return new Car(numberCar, nameCar, color, mileage, price);
    };

    @Override
    public List<Car> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(carMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccesfulWorkWithFileException(e);
        }

    }

    @Override
    public double findAveragePriceCamry() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getNameCar().equals("Camry"))
                    .mapToInt(Car::getPrice)
                    .average()
                    .getAsDouble();

        } catch (IOException e) {
            throw new UnsuccesfulWorkWithFileException(e);
        }

    }

    @Override
    public List<String> getNumberCarWithPriceAndColorOption() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getColor().equals("Black") || car.getMileage() == 0)
                    .map(car -> car.getNumberCar())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccesfulWorkWithFileException(e);
        }
    }

    @Override
    public String getColorMinPriceCar() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(carMapper)
                    .min((car1, car2) -> car1.getPrice() - car2.getPrice())
                    .get().getColor();
        } catch (IOException e) {
            throw new UnsuccesfulWorkWithFileException(e);
        }
    }

    @Override
    public List<String> uniqueCarFromRangePrice() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getPrice() <= 80000 && car.getPrice() >= 7000)
                    .map(Car::getNameCar)
                    .distinct()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccesfulWorkWithFileException(e);
        }
    }
}
