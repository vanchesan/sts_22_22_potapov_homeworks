public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFileBaseImpl("input.txt");
        System.out.println(carsRepository.findAll());
        System.out.println(carsRepository.findAveragePriceCamry());
        System.out.println(carsRepository.getNumberCarWithPriceAndColorOption());
        System.out.println(carsRepository.uniqueCarFromRangePrice());
        System.out.println(carsRepository.getColorMinPriceCar());
    }
}
