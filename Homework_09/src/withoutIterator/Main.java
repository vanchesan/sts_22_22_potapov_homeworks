package withoutIterator;

public class Main {
    public static void main(String[] args) {

        System.out.println("LinkedList");
        List<Integer> test = new LinkedList<>();
        test.add(2);
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.add(1);
        test.add(6);
        for (int i = 0; i < test.size(); i++) {
            System.out.print(test.get(i) + " ");
        }
        System.out.println();

        System.out.println("�������� ��������� ��� �������� 0");
        test.removeAt(0);
        for (int i = 0; i < test.size(); i++) {
            System.out.print(test.get(i) + " ");
        }
        System.out.println();

        System.out.println("�������� ������� ��������������");
        test.remove(1);
        for (int i = 0; i < test.size(); i++) {
            System.out.print(test.get(i) + " ");
        }

        System.out.println();
        System.out.println("������� �� � ����� ������� �� ��������� 4 " + test.contains(4));
        System.out.println("������� �� � ����� ������� �� ��������� 400 " + test.contains(400));

        System.out.println("ArrayList");
        List<Integer> test1 = new ArrayList<>();
        test1.add(0);
        test1.add(15);
        test1.add(2);
        test1.add(2);
        test1.add(2);
        test1.add(1);
        test1.add(6);
        for (int i = 0; i < test1.size(); i++) {
            System.out.print(test1.get(i) + " ");
        }
        System.out.println();
        System.out.println("�������� �������� ��� �������� 6");
        test1.removeAt(6);
        for (int i = 0; i < test1.size(); i++) {
            System.out.print(test1.get(i) + " ");
        }
        System.out.println();

        System.out.println("�������� ������� ��������������");
        test1.remove(2);
        for (int i = 0; i < test1.size(); i++) {
            System.out.print(test1.get(i) + " ");
        }


        System.out.println();
        System.out.println("������� �� � ����� ������� �� ��������� 4 " + test1.contains(-2));
        System.out.println("������� �� � ����� ������� �� ��������� 40 " + test1.contains(40));
    }


}

