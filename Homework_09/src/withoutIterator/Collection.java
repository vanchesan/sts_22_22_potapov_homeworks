package withoutIterator;

public interface Collection<T> {
    /**
     * ��������� ������� � ���������
     *
     * @param element ����������� �������
     */
    void add(T element);

    /**
     * ������� ������� �� ��������� � ������� <code>boolean equals()</code>
     *
     * @param element ��������� �������
     */
    void remove(T element);

    /**
     * ��������� ������� �������� � ���������
     *
     * @param element ������� �������
     * @return <code>true</code>, ���� ������� ������
     * <code>false</code> ���� ������� �� ������
     */
    boolean contains(T element);

    /**
     * ���������� ���������� ��������� � ���������
     *
     * @return �����, ������ ���������� ����������� ���������
     */
    int size();
}
