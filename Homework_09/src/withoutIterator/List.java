package withoutIterator;


public interface List<E> extends Collection<E> {
    /**
     * ������� ������� �� �������
     *
     * @param index ������ ��������
     */
    void removeAt(int index);

    /**
     * �������� ������� �� �������
     *
     * @param index ������ ��������
     * @return ��������, ������� ����� ��� ��������, ���� <code>null</code>
     */
    E get(int index);
}
