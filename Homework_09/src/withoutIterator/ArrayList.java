package withoutIterator;

public class ArrayList<T> implements List<T> {

    private final static int DEFAULT_ARRAY_SIZE = 10;

    // ����-������, ������� ������ ��� ��������
    private T[] elements;

    // ����, �������� ������ ����� ���������
    private int count;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    @Override
    public void add(T element) {
        // ���� ������ ��������
        if (isFull()) {
            // �������� ��� ������
            resize();
        }
        elements[count] = element;
        count++;
    }

    private void resize() {
        // ����� ������ ������ �������
        int currentLength = elements.length;
        // �������� ��������, � ������� ���� ������ ������������ ������f
        // ��� � ����� ����� ������
        int newLength = currentLength + currentLength / 2;
        // ������� ����� ������ �������� �������
        T[] newElements = (T[]) new Object[newLength];
        // ��������������� �������� ��������
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        // ����������� ������ �� ����� ������
        this.elements = newElements;
    }

    private boolean isFull() {
        return count == elements.length;
    }

    @Override
    public void remove(T element) {
        int number = 0;
        int index = 0;
        for (int i = count - 1; i >= 0; i--) {
            if (elements[i].equals(element)) {
                number++;
               if (number >1) {
                index = i;}
            }
        }

        System.out.println(index);
        removeAt(index);
    }

    @Override
    public boolean contains(T element) {
        // ��������� ��� ��������
        for (int i = 0; i < count; i++) {
            // ���� ������� ������
            if (elements[i].equals(element)) {
                return true;
            }
        }
        // ���� ������� ��� � �� ��� ������ - ���������� false
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {
        if (index >= 0 && index < count) {
            T[] newArrays = (T[]) new Object[elements.length];
            for (int i = 0; i < count; i++) {
                newArrays[i] = elements[i];
            }
            for (int i = count - 1; i >= index; i--) {
                newArrays[i] = elements[i + 1];
            }
            //newArrays[count] = null;

            this.elements = newArrays;
            count--;
        } else {
            System.out.println("�������� ������");
        }

    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        }
        return null;
    }


}
