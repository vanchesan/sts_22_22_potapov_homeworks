package withoutIterator;

public class LinkedList<T> implements List<T> {

    private Node<T> first;

    private Node<T> last;
    private int count;

    private static class Node<E> {

        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }

    }

    //    @Override
//    public void add(T element) {
//        // ������� ����� ���� �� ���������
//        Node<T> newNode = new Node<>(element);
//        // ���� ��������� � ������ ���
//        if (count == 0) {
//            // ������ ����� ���� � �������� �������
//            this.first = newNode;
//        } else {
//            // ���� � ������ ��� ���� ����, ����� ��������� �� ����������
//
//            // ���������� ������ �� ������ ����
//            Node<T> current = this.first;
//            // ���� �� ������ �� ����, � �������� ��� ���������� ���� (�� ����������)
//            while (current.next != null) {
//                current = current.next;
//            }
//            // ������ � ��� current ��������� �� ��������� ����
//
//            // ������ ��������� ����� ���������� - ����� ����
//            current.next = newNode;
//        }
//        count++;
//    }
    @Override
    public void add(T element) {
        // ������� ����� ���� �� ���������
        Node<T> newNode = new Node<>(element);
        // ���� ��������� � ������ ���
        if (count == 0) {
            // ������ ����� ���� � �������� ������� � ����������
            this.first = newNode;
        } else {
            // ���� � ������ ��� ���� ����
            // � ���������� ������ ��������� ����� ����
            this.last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    @Override
    public void remove(T element) {

        Node<T> prev = null;
        Node<T> current = this.first;
        int number = 0;
        int index = 0;
        for (int i = count; i >= 0; i--) {
            if (current.value.equals(element)) {
                number++;
                if (number > 2) {
                    index = i;
                }
            }

        }
        removeAt(index);
//        while (current != null) {
//            if (current.value == element) {
//                number++;
//                if (number >2){
//                if (prev != null) {
//                    prev.next = current.next;
//                    if (current.next == null) {
//                        this.last = prev;
//                    }
//                } else {
//                    current = this.first.next;
//                    if (this.first == null) {
//                        this.last = null;
//                    }
//                }
//                count--;
//            }}
//            prev = current;
//            current = current.next;
//        }
    }

    @Override
    public boolean contains(T element) {
        // �������� ������ �� ������ ������� ������
        // �������� �� ���� ��������� ������
        Node<T> current = this.first;
        // ���� �� ������ ���� ������
        while (current != null) {
            // ���� �������� � ������� ���� ������� � �������
            if (current.value.equals(element)) {
                return true;
            }
            // ���� �� ������� - ���� � ���������� ����
            current = current.next;
        }
        // ���� �� ����� �������
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {
        Node<T> prev = null;
        Node<T> current = this.first;
        if (0 <= index && index <= count) {
            int i = 0;
            while (current != null) {
                if (i == index) {
                    if (prev != null) {
                        prev.next = current.next;
                        if (current.next == null) {
                            this.last = prev;
                        }
                    } else {
                        this.first = this.first.next;
                        current = this.first;
                        if (this.first == null) {
                            this.last = null;
                        }
                    }
                    count--;

                }
                prev = current;
                current = current.next;
                i++;

            }

        }
    }


    @Override
    public T get(int index) {
        if (0 <= index && index <= count) {
            // �������� � ������� ��������
            Node<T> current = this.first;

            // ���� ����������� ������ ��� ����� - 3
            // 0, 1, 2
            for (int i = 0; i < index; i++) {
                // �� ������ ���� ����� ��������� ������
                current = current.next;
            }
            return current.value;
        }
        return null;

    }

}