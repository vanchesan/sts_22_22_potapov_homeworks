public class Main {
    public static void main(String[] args) {
        // ��������� ������ ������ � ����� ���������� ������
        Figure obj = new Figure(0, 0);
        System.out.println(obj.getX() + " " + obj.getY());
        obj.move(7, 8);
        System.out.println(obj.getX() + " " + obj.getY());

        //���������� ������ � ������ ������������� (�� ��������)
        Rectangle rect = new Rectangle(0, 0, -1, -2, 1, 2);
        System.out.println("���������� ��������������");
        System.out.println(rect.getX1() + " " + rect.getY1() + " " + rect.getX2() + " " + rect.getY2());
        System.out.println(rect.Square());
        System.out.println(rect.Perimeter());
        rect.setX1(4);
        rect.setY1(4);
        rect.setX2(0);
        rect.setY2(2);
        System.out.println(rect.getX1() + " " + rect.getY1() + " " + rect.getX2() + " " + rect.getY2());
        rect.move(10, 10);


        // ���������� ������ � ������ �������� (�� ��������)
        Squart squart = new Squart(0, 0, -1, 1, 1, -1);
        System.out.println("���������� ��������");
        System.out.println(squart.getX1() + " " + squart.getY1() + " " + squart.getX2() + " " + squart.getY2());
        System.out.println(squart.Square());
        System.out.println(squart.Perimeter());
        squart.setX1(-6);
        squart.setY1(6);
        squart.setX2(0);
        squart.setY2(0);
        System.out.println(squart.getX1() + " " + squart.getY1() + " " + squart.getX2() + " " + squart.getY2());
        squart.move(10, 10);


        //���������� ������ � ������ ����� ������
        Ellipse ellipse = new Ellipse(0, 0, -2, 3, 2, 3);
        System.out.println(ellipse.getX1() + " " + ellipse.getY1() + " " + ellipse.getX2() + " " + ellipse.getY2());
        System.out.println("���������� �������");
        System.out.println(ellipse.Square());
        System.out.println(ellipse.Perimeter());
        ellipse.setX1(4);
        ellipse.setY1(4);
        ellipse.setX2(-4);
        ellipse.setY2(2);
        System.out.println(ellipse.getX1() + " " + ellipse.getY1() + " " + ellipse.getX2() + " " + ellipse.getY2());
        ellipse.move(10, 10);


        //������ ����� � ���������
        Circle circle = new Circle(0, 0, -2, 2, 2, -2);
        System.out.println("���������� �����");
        System.out.println(circle.getX1() + " " + circle.getY1() + " " + circle.getX2() + " " + circle.getY2());
        System.out.println(circle.Square());
        System.out.println(circle.Perimeter());
        circle.setX1(-4);
        circle.setY1(-4);
        circle.setX2(-2);
        circle.setY2(-2);
        System.out.println(circle.getX1() + " " + circle.getY1() + " " + circle.getX2() + " " + circle.getY2());
        circle.move(10, 10);

    }
}
