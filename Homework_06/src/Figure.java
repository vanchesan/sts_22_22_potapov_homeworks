public class Figure {
    private int x, y, x1, y1, x2, y2;

    Figure (int x, int y){}
    Figure(int x, int y, int x1, int y1, int x2, int y2) {
        this.x = x;
        this.y = y;
        this.x1= x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2= y2;

    }

    public boolean isCorrect(int arg) {
        return (-100 <= arg && arg <= 100);
    }

    public void setX(int x) {
        if (isCorrect(x)) {
            this.x = x;
        }
    }

    public void setY(int y) {
        if (isCorrect(y)) {
            this.y = y;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public void setX1(int x1) {
        if (isCorrect(x1)) {
            this.x1 = x1;
        }
    }

    public void setY1(int y1) {
        if (isCorrect(y1)) {
            this.y1 = y1;
        }
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public boolean isEquals() {
        return (Math.abs(getX1() - getX2()) == Math.abs(getY1() - getY2()));
    }
    public void move(int toX, int toY) {
        int x1M = (toX - Math.abs(getX1() - getX2()) / 2);
        int y1M = (toY - Math.abs(getY1() - getY2()) / 2);
        int x2M = (Math.abs(getX1() - getX2()) / 2 + toX);
        int y2M = (Math.abs(getY1() - getY2()) / 2 + toY);
        System.out.println("���������� ������ ������: X: " + toX + " Y: " + toY);
        System.out.println("����� ���������� ������");
        System.out.println("X1 " + x1M + " Y1 " + y1M + " X2 " + x2M + " Y2 " + y2M);
    }

}
