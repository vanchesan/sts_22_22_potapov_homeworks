import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
public class Method {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("������� ������ ���������");
        double from = scanner.nextDouble();
        System.out.println("������� ����� ���������");
        double to = scanner.nextDouble();
        System.out.println("������� ���");
        double step = scanner.nextDouble();
        double sum = calcSum(from, to, step);
        System.out.println("����� ��������: " + sum);

        System.out.println("������� �������� ��������� ����� � ������ �������");
        int bound = scanner.nextInt();
        int arrayLength = scanner.nextInt();
        int[] array = generateRandomArray(bound, arrayLength);
        System.out.println("�������� �������������� ������");
        System.out.println(Arrays.toString(array));
        showEvenNumberInArray(array);
        System.out.println();

        System.out.println("������� ������ �������");
        int length = scanner.nextInt();
        System.out.println("������� �������� �������");
        int[] number = new int[length];
        for (int i = 0; i < length; i++) {
            number[i] = scanner.nextInt();
        }
        int result = toInt(number);
        System.out.println("����� ������������� �� �������: " + result);
    }

       public static double calcSum(double from, double to, double step) {
        double sum = 0;
        if (from > to) {
            return -1.0;
        }
        from = from * Math.pow(10, 3);
        to = to * Math.pow(10, 3);
        step = step * Math.pow(10, 3);

        for (double i = from; i <= to; i += step) {
            System.out.print(i * Math.pow(10, -3) + " ");
            sum += i;
        }
        System.out.println();
        return sum * Math.pow(10, -3);
    }

    public static void showEvenNumberInArray(int[] array) {
        System.out.println("������ �������� �������");
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                System.out.print(array[i] + " ");
            }
        }
    }

    public static int toInt(int[] number) {
        int result = 0;
        int count = 0;
        for (int i = 0; i < number.length; i++) {
            result += Math.abs(number[i]) * Math.pow(10, (number.length - 1) - i);
            if (number[i] < 0) {
                count++;
            }

        }
        if (count % 2 == 0) {
            return result;
        }
        return result * (-1);
    }

    public static int[] generateRandomArray(int bound, int arrayLength) {
        Random random = new Random();
        int[] result = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            result[i] = random.nextInt(bound);
        }
        return result;
    }

}
