import java.util.Scanner;

public class ATM {
    Scanner scanner = new Scanner(System.in);
    private int remainSumMoney; // ����� ����� � ��������� � ������� ������
    private int MAX_GET_MONEY = 50000; // ������������ ����� ��������/������ �� ��������
    private int MAX_SUM_MONEY = 50000; //������������ ����� ����� � ���������
    private int countOperation = 0; // ������� ��������
    private int money; // �������� �����

    public ATM(int remainSumMoney, int MAX_GET_MONEY, int MAX_SUM_MONEY, int countOperation) {
        this.remainSumMoney = remainSumMoney;
        this.MAX_GET_MONEY = MAX_GET_MONEY;
        this.MAX_SUM_MONEY = MAX_SUM_MONEY;
        this.countOperation = countOperation;
    }

    // �������� ������������ ������� �����
    private boolean isCorrect(int money) {
        return (0 < money && money <= MAX_GET_MONEY);
    }

    //����� �������� �����
    public void setMoney(int money) {
        if (isCorrect(money)) {
            this.money = money;
        } else {
            System.out.println("�������� �����");
        }
    }

    //������ �������� �����
    public int giveMoney() {
        int sum = 0;
        if (remainSumMoney < money) {
            money = remainSumMoney;
            remainSumMoney = 0;
            counter();
            System.out.println("����� " + money);
            System.out.println("�������� " + remainSumMoney);
        } else {
            remainSumMoney -= money;
            System.out.println("����� " + money);
            System.out.println("�������� " + remainSumMoney);
            sum+=remainSumMoney;
            counter();
            return remainSumMoney;
        }

        return remainSumMoney;
    }

    //�������� ������ � ��������
    public int putMoney() {
        if ((remainSumMoney + money) <= MAX_SUM_MONEY) {
            System.out.println("������� " + money);
            remainSumMoney += money;
            System.out.println("�������� " + remainSumMoney);
            counter();
            return remainSumMoney;
        } else {
            int temp = money;
            money = MAX_SUM_MONEY - remainSumMoney;
            remainSumMoney = MAX_SUM_MONEY;
            System.out.println("������� " + money);
            System.out.println("�������� " + remainSumMoney);
            System.out.println("����������� ������� " + (temp - money));
            counter();
            return remainSumMoney;
        }
    }

    // ������ ��������
    public void counter() {
        countOperation++;
        System.out.println("����� �������� - " + countOperation);
    }

}

