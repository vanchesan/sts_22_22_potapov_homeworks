package work;

public class Main {
    public static void main(String[] args) {
        int[] array1 = {4, -4, -100, 5, 9, -7, 954,};
        int[] array2 = {123, 10, 78, -120, 0};


        SetRange range1 = new SetRange(array1, 0, array1.length - 1);
        range1.setFrom(0);
        range1.setTo(6);

        SetRange range2 = new SetRange(array2, 0, array2.length - 1);
        ArrayTask taskSum = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            return sum;
        };
        ArrayTask taskMaxNumberSum = (array, from, to) -> {
            int number = 0;
            for (int i = from; i <= to; i++) {
                if (number < array[i]) {
                    number = array[i];
                }
            }
            int newNumber;
            int sumNumber = 0;
            do {
                newNumber = number % 10;
                number /= 10;
                sumNumber += newNumber;
            } while (number > 0);
            return sumNumber;
        };
        ArraysTaskResolver.resolveTask(array1, taskSum, range1.getFrom(), range1.getTo());
        ArraysTaskResolver.resolveTask(array2, taskMaxNumberSum, range2.getFrom(), range2.getTo());
    }
}

