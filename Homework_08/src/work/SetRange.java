package work;

public class SetRange {
    private int from, to;
    private int[] array;

    public SetRange(int[] array, int from, int to) {
        this.array = array;
        this.from = from;
        this.to = to;
    }

    public boolean isCorrect(int arg) {
        if ((arg >= 0 && arg < array.length)) {
            return true;
        } else {
            System.out.println("����� �� �������");
            return false;
        }
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        isCorrect(from);
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        isCorrect(to);
        this.to = to;
    }
}
