package ru.inno.cars.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarsRepositoryJdbcImpl;
import ru.inno.cars.repository.CarsRepository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


import java.util.Properties;
import java.util.Scanner;

@Parameters(separators = "=")
public class Main {
    @Parameter(names = {"--action"})
    private String action;

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        Main main = new Main();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);


        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        CarsRepository carsRepository = new CarsRepositoryJdbcImpl(dataSource);
        char exit;
        do {
            System.out.println("For exit push \"q\"");
            if (main.action.equals("read")) {
                System.out.println(carsRepository.findAll());
            }
            if (main.action.equals("write")) {
                buildCar(scanner, carsRepository);
            }
            exit = (char) System.in.read();
        } while (exit != 'q');
        scanner.nextLine();
        System.exit(0);
    }

    private static void buildCar(Scanner scanner, CarsRepository carsRepository) {
        System.out.println("Model");
        String model = scanner.nextLine();
        System.out.println("Color");
        String color = scanner.nextLine();
        System.out.println("Car number");
        String numberCar = scanner.nextLine();
        System.out.println("Driver ID");
        Long driverId = scanner.nextLong();
        Car car = Car.builder()
                .model(model)
                .color(color)
                .numberCar(numberCar)
                .driverId(driverId)
                .build();
        System.out.println(car);
        carsRepository.save(car);
        System.out.println(carsRepository.findAll());
        scanner.nextLine();
    }
}
