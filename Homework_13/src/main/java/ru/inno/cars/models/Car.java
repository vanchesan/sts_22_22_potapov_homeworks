package ru.inno.cars.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Car {
    private long id;
    private String model;
    private String color;
    private String numberCar;
    private long driverId;

}
