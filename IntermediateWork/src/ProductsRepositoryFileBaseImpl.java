import java.io.*;
import java.util.List;
import java.util.function.Function;

public class ProductsRepositoryFileBaseImpl implements ProductsRepository {
    private final String fileName;

    public ProductsRepositoryFileBaseImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> productMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double price = Double.parseDouble(parts[2]);
        Integer count = Integer.parseInt(parts[3]);
        return new Product(id, name, price, count);
    };
    private static final Function<Product, String> productToStringMapper = product ->
            product.getId() + "|" + product.getName() + "|" + product.getPrice() + "|" + product.getCount();


    @Override
    public Product findById(Integer id) {
        if (id > 0) {
            try (Reader fileReader = new FileReader(fileName);
                 BufferedReader bufferedReader = new BufferedReader(fileReader)) {
                return bufferedReader
                        .lines()
                        .map(productMapper)
                        .filter(product -> product.getId() == id)
                        .findFirst().orElse(null);
            } catch (IOException e) {
                throw new UnsuccessfulWorkWithFileException(e);
            }
        } else return null;

    }

    @Override
    public List<Product> findByTitleLike(String title) {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(productMapper)
                    .filter(product -> product.getName().toLowerCase().contains(title) || product.getName().toUpperCase().contains(title))
                    .toList();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public void update(Product product) {
        try {
            List<Product> products = new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(productMapper)
                    .toList();
            for (Product update : products) {
                if (update.getId() == product.getId()) {
                    update.setPrice(product.getPrice());
                    update.setCount(product.getCount());
                    productWriter(products);
                }

            }
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    private void productWriter(List<Product> products) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));
        for (Product current : products) {
            String productToSave = productToStringMapper.apply(current);
            bufferedWriter.write(productToSave);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }

}
