import java.io.FileNotFoundException;
import java.util.List;

public interface ProductsRepository {
    Product findById(Integer id);

    List<Product> findByTitleLike(String title);

    void update(Product product) throws FileNotFoundException;
}
