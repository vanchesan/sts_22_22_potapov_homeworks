import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        ProductsRepository productsRepository = new ProductsRepositoryFileBaseImpl("Resources/ListProducts.txt");
        System.out.println(productsRepository.findById(1).getName());
        System.out.println(productsRepository.findByTitleLike("�"));
        System.out.println(productsRepository.findById(5));
        Product abricos = productsRepository.findById(1);
        abricos.setPrice(145.9);
        abricos.setCount(7);
        System.out.println(abricos);
        productsRepository.update(abricos);
    }
}